<?php

namespace Drupal\creathing_site_settings\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Form\SiteInformationForm;


class CreathingSiteSettingsForm extends SiteInformationForm {

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $site_config = $this->config('system.site');
        $form =  parent::buildForm($form, $form_state);

        $form['site_information']['headscripts'] = [
            '#type' => 'textarea',
            '#title' => t('Head scripts'),
            '#default_value' => $site_config->get('headscripts') ?: '',
            //			'#description' => t("IBP Key"),
        ];

        $form['site_information']['bodyscripts'] = [
            '#type' => 'textarea',
            '#title' => t('Body scripts'),
            '#default_value' => $site_config->get('bodyscripts') ?: '',
            //			'#description' => t("IBP Key"),
        ];

        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this
                ->t('Submit'),
        ];

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        $this->config('system.site')
            ->set('headscripts', $form_state->getValue('headscripts'))
            ->set('bodyscripts', $form_state->getValue('bodyscripts'))
            ->save();
        parent::submitForm($form, $form_state);
    }
}
