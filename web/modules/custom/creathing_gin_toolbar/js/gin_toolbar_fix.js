document.addEventListener('DOMContentLoaded', () => {
  const adminAnchors = document.querySelectorAll(
    '#toolbar-administration .menu-item--expanded > a'
  );

  adminAnchors.forEach(item => {
    item.addEventListener('click', e => {
      if (e.target.parentNode.querySelectorAll('ul.toolbar-menu').length > 0) {
        e.preventDefault();
      }
    });
  });
});
