/* eslint-disable camelcase */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/jsx-curly-spacing */
/* eslint-disable object-shorthand */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-indent */
/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */

/**
 * SECTION IMPORTS
 */
const { blocks, data, element, components, editor, blockEditor } = wp;
const { registerBlockType } = blocks;
const { dispatch, select } = data;
const { Fragment } = element;
const {
  PanelBody,
  BaseControl,
  Icon,
  RangeControl,
  IconButton,
  Toolbar,
  SelectControl,
} = components;
const {
  InnerBlocks,
  RichText,
  InspectorControls,
  PanelColorSettings,
  MediaUpload,
  BlockControls,
  AlignmentToolbar,
  URLInputButton,
  PlainText,
} = blockEditor;
const __ = Drupal.t;

/**
 * SECTION SETTINGS
 */

// const testBlockSettings = {
//   title: __('Test'),
//   description: __('Custom CreaThing block for making "Tests"'),
//   icon: 'editor-expand',
//   attributes: {

//   },
//   edit({ className, attributes, setAttributes }) {},
//   save() {}
// }

const faqBlockSettings = {
  title: __('FAQ'),
  description: __(`Custom CreaThing block for making FAQ's`),
  icon: 'testimonial',
  attributes: {
    question: {
      type: 'string',
    },
  },

  edit({ className, attributes, setAttributes }) {
    const { question } = attributes;

    return (
      <Fragment>
        <div
          className={className}
          style={{ backgroundColor: '#f1f1f1', padding: '5px 25px' }}
        >
          <RichText
            identifier="question"
            tagName="h4"
            value={question}
            placeholder={__('Vraag')}
            onChange={nextQuestion => {
              setAttributes({
                question: nextQuestion,
              });
            }}
            onSplit={() => null}
            unstableOnSplit={() => null}
          />

          <InnerBlocks allowedBlocks={['core/paragraph', 'core/buttons']} />
        </div>
        <InspectorControls>
          <PanelBody title={__('Block Settings')}>
            <div>{question}</div>
          </PanelBody>
        </InspectorControls>
      </Fragment>
    );
  },

  save({ className, attributes }) {
    const { question } = attributes;

    return (
      <div className={className}>
        {question && <h4>{question}</h4>}

        <div className="faq-answer">
          <InnerBlocks.Content />
        </div>
      </div>
    );
  },
};
// const billboardBlockSettings = {
//   title: __('Billboard'),
//   description: __(`Custom CreaThing block for making "Billboards"`),
//   icon: 'editor-expand',
//   attributes: {
//     title: {
//       type: 'string',
//     },
//     mediaID: {
//       type: 'number',
//     },
//     mediaURL: {
//       type: 'string',
//       source: 'attribute',
//       selector: 'img',
//       attribute: 'src',
//     },
//     text: {
//       type: 'string',
//     },
//     url: {
//       type: 'string',
//     },
//     urlText: {
//       type: 'string',
//     },
//     alignment: {
//       type: 'string',
//     },
//     color: {
//       type: 'string',
//     },
//   },

//   edit({ className, attributes, setAttributes, isSelected }) {
//     const { title, text, mediaID, mediaURL, alignment, color } = attributes;

//     const onSelectImage = media => {
//       setAttributes({
//         // mediaURL: media.url,
//         mediaURL: media.media_details.sizes.billboard.source_url,
//         mediaID: media.id,
//       });
//     };

//     const ALLOWED_BLOCKS = ['core/paragraph', 'core/buttons'];

//     return (
//       <Fragment>
//         {isSelected && (
//           <BlockControls key="controls">
//             <AlignmentToolbar
//               value={alignment}
//               onChange={nextAlign => {
//                 setAttributes({ alignment: nextAlign });
//               }}
//             />
//           </BlockControls>
//         )}

//         <div className={`${className} align-content-${alignment}`}>
//           <div className="billboard-image">
//             <MediaUpload
//               onSelect={onSelectImage}
//               type="image"
//               value={mediaID}
//               render={({ open }) => (
//                 <button
//                   type="button"
//                   className={
//                     mediaID
//                       ? 'image-button'
//                       : 'components-button block-editor-media-placeholder__button block-editor-media-placeholder__upload-button is-primary'
//                   }
//                   onClick={open}
//                 >
//                   {!mediaID ? (
//                     __('Upload Image')
//                   ) : (
//                     <img alt="" src={mediaURL} />
//                   )}
//                 </button>
//               )}
//             />
//           </div>

//           <div className={`billboard-content has-${color}-background-color`}>
//             <RichText
//               identifier="title"
//               tagName="h2"
//               value={title}
//               placeholder={__('Titel')}
//               onChange={nextTitle => {
//                 setAttributes({
//                   title: nextTitle,
//                 });
//               }}
//               onSplit={() => null}
//               unstableOnSplit={() => null}
//             />

//             <RichText
//               identifier="text"
//               tagName="p"
//               value={text}
//               placeholder={__('Text')}
//               onChange={nextText => {
//                 setAttributes({
//                   text: nextText,
//                 });
//               }}
//               onSplit={() => null}
//               unstableOnSplit={() => null}
//             />

//             <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} />
//           </div>
//         </div>
//         <InspectorControls>
//           {/* <PanelBody title={__('Block Settings')}>
//             <div>{title}</div>
//           </PanelBody> */}
//           <PanelColorSettings
//             title={__('Kleuren')}
//             colors={[]}
//             colorSettings={[
//               {
//                 value: color,
//                 onChange: nextColor => {
//                   switch (nextColor) {
//                     case '#000000':
//                       setAttributes({
//                         color: 'black',
//                       });
//                       break;
//                     case '#ff5454':
//                       setAttributes({
//                         color: 'pink',
//                       });
//                       break;
//                     case '#522db2':
//                       setAttributes({
//                         color: 'purple',
//                       });
//                       break;
//                     case '#3dbb3a':
//                       setAttributes({
//                         color: 'green',
//                       });
//                       break;

//                     default:
//                       break;
//                   }
//                 },
//                 label: 'Achtergrond',
//                 disableCustomColors: true,
//                 colors: [
//                   {
//                     slug: 'black',
//                     name: 'Black',
//                     color: '#000000',
//                   },
//                   {
//                     slug: 'pink',
//                     name: 'Pink',
//                     color: '#ff5454',
//                   },
//                   {
//                     slug: 'purple',
//                     name: 'Purple',
//                     color: '#522db2',
//                   },
//                   {
//                     slug: 'green',
//                     name: 'Green',
//                     color: '#3dbb3a',
//                   },
//                 ],
//               },
//             ]}
//           />
//         </InspectorControls>
//       </Fragment>
//     );
//   },

//   save({ className, attributes }) {
//     const {
//       title,
//       text,
//       mediaURL,
//       alignment,
//       color,
//       url,
//       urlText,
//     } = attributes;

//     return (
//       <div className={`${className} align-content-${alignment}`}>
//         <div className="billboard-image">
//           {mediaURL && (
//             <img data-image-style="billboard" alt="" src={mediaURL} />
//           )}
//         </div>
//         <div className={`billboard-content has-${color}-background-color`}>
//           <div className="billboard-content-inner">
//             {title && <h2>{title}</h2>}
//             {text && <p>{text}</p>}
//             <InnerBlocks.Content />
//           </div>
//         </div>
//       </div>
//     );
//   },
// };

// const testBlockSettings = {
//   title: __('Test'),
//   description: __('Custom CreaThing block for making "Tests"'),
//   icon: 'editor-expand',
//   attributes: {
//     naam_medewerker: {
//       type: 'string',
//     },
//     email_medewerker: {
//       type: 'string',
//     },
//     intro_medewerker: {
//       type: 'string',
//     },
//   },
//   edit({ className, attributes, setAttributes }) {
//     const { naam_medewerker, email_medewerker, intro_medewerker } = attributes;

//     return (
//       <Fragment>
//         <div className={`${className}`}>
//           <div className="information-header">
//             <RichText
//               identifier="naam_medewerker"
//               tagName="h2"
//               value={naam_medewerker}
//               placeholder={__('Naam medewerker')}
//               onChange={nextName => {
//                 setAttributes({
//                   naam_medewerker: nextName,
//                 });
//               }}
//               onSplit={() => null}
//               unstableOnSplit={() => null}
//             />

//             <RichText
//               identifier="email_medewerker"
//               tagName="p"
//               value={email_medewerker}
//               placeholder={__('Email medewerker')}
//               onChange={nextEmail => {
//                 setAttributes({
//                   email_medewerker: nextEmail,
//                 });
//               }}
//               onSplit={() => null}
//               unstableOnSplit={() => null}
//             />
//           </div>
//           <div className="information-body">
//             <RichText
//               identifier="intro_medewerker"
//               tagName="p"
//               value={intro_medewerker}
//               placeholder={__('Introductie medewerker')}
//               onChange={nextIntro => {
//                 setAttributes({
//                   intro_medewerker: nextIntro,
//                 });
//               }}
//               onSplit={() => null}
//               unstableOnSplit={() => null}
//             />

//             <InnerBlocks allowedBlocks={['core/paragraph', 'core/buttons']} />
//           </div>
//         </div>
//       </Fragment>
//     );
//   },
//   save({ className, attributes }) {
//     const { naam_medewerker, email_medewerker, intro_medewerker } = attributes;
//     return (
//       <div className={`${className}`}>
//         <div className="information-header">
//           <h2>{naam_medewerker}</h2>
//           <p>{email_medewerker}</p>
//         </div>
//         <div className="information-body">
//           <p>{intro_medewerker}</p>
//           <InnerBlocks.Content />
//         </div>
//       </div>
//     );
//   },
// };

const customCTABlockSettings = {
  title: __('Custom CTA'),
  description: __(`Custom CreaThing block for making "Custom CTA's"`),
  icon: 'format-aside',
  attributes: {
    title: {
      type: 'string',
    },
  },

  edit({ className, attributes, setAttributes }) {
    const { title } = attributes;

    const ALLOWED_BLOCKS = ['core/paragraph', 'core/buttons'];

    return (
      <Fragment>
        <div
          className={`${className}`}
          style={{ backgroundColor: '#F7F7F2', padding: '20px' }}
        >
          <RichText
            identifier="title"
            tagName="h2"
            value={title}
            placeholder={__('Titel')}
            onChange={nextTitle => {
              setAttributes({
                title: nextTitle,
              });
            }}
            onSplit={() => null}
            unstableOnSplit={() => null}
          />

          <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} />
        </div>
      </Fragment>
    );
  },

  save({ className, attributes }) {
    const { title } = attributes;

    return (
      <div className={`${className}`}>
        <div className="accolade-content-wrapper">
          {title && (
            <div className="accolade-content-title">
              <h2>{title}</h2>
            </div>
          )}
          <div className="accolade-content-inner">
            <InnerBlocks.Content />
          </div>
        </div>
      </div>
    );
  },
};

/**
 * SECTION REGISTRATION
 */

const category = {
  slug: 'creablocks',
  title: __('CreaThing Blocks'),
};

const currentCategories = select('core/blocks')
  .getCategories()
  .filter(item => item.slug !== category.slug);
dispatch('core/blocks').setCategories([category, ...currentCategories]);

registerBlockType(`${category.slug}/creathing-faq`, {
  category: category.slug,
  ...faqBlockSettings,
});

// registerBlockType(`${category.slug}/creathing-billboard`, {
//   category: category.slug,
//   ...billboardBlockSettings,
// });

// registerBlockType(`${category.slug}/creathing-test`, {
//   category: category.slug,
//   ...testBlockSettings,
// });

registerBlockType(`${category.slug}/custom-cta`, {
  category: category.slug,
  ...customCTABlockSettings,
});
