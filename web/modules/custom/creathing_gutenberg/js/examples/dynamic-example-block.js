/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/jsx-curly-spacing */
/* eslint-disable object-shorthand */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-indent */
/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */

const { blocks, data, element, components, editor } = wp;
const { registerBlockType } = blocks;
const { dispatch, select } = data;
const { Fragment } = element;
const {
  PanelBody,
  BaseControl,
  Icon,
  RangeControl,
  IconButton,
  Toolbar,
  SelectControl,
} = components;
const {
  InnerBlocks,
  RichText,
  InspectorControls,
  PanelColorSettings,
  MediaUpload,
  BlockControls,
} = editor;
const __ = Drupal.t;

const dynamicExampleBlock = {
  title: __('Gutenberg Example Dynamic Block'),
  description: __(
    'Gutenberg example dynamic block that can be rendered server-side.',
  ),
  icon: 'welcome-learn-more',
  attributes: {
    title: {
      type: 'string',
    },
  },

  edit({ className, attributes, setAttributes, isSelected }) {
    const { title } = attributes;

    return (
      <div className={className}>
        <div>— Hello from the Gutenberg JS editor.</div>
        <div className="dynamic-block-title">
          <RichText
            identifier="title"
            tagName="h2"
            value={title}
            placeholder={__('Title')}
            onChange={title => {
              setAttributes({
                title: title,
              });
            }}
            onSplit={() => null}
            unstableOnSplit={() => null}
          />
        </div>
        <div className="dynamic-block-content">
          <InnerBlocks />
        </div>
      </div>
    );
  },

  save({ className, attributes }) {
    const { title } = attributes;

    // Save the inner content block.
    return <InnerBlocks.Content />;
  },
};

const category = {
  slug: 'creablocks',
  title: __('CreaThing Blocks'),
};

const currentCategories = select('core/blocks')
  .getCategories()
  .filter(item => item.slug !== category.slug);
dispatch('core/blocks').setCategories([category, ...currentCategories]);

registerBlockType(`${category.slug}/dynamic-block`, {
  category: category.slug,
  ...dynamicExampleBlock,
});
