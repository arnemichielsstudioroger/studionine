(function ($) {
  $.fn.doubleTapToGo = function () {
    if (
      !('ontouchstart' in window) &&
      !navigator.msMaxTouchPoints &&
      !navigator.userAgent.toLowerCase().match(/windows phone os 7/i)
    )
      return false;

    this.each(function () {
      var curItem = false;

      $(this).on('click', function (e) {
        var item = $(this);
        if (item[0] != curItem[0]) {
          e.preventDefault();
          curItem = item;
        }
      });

      $(document).on('click touchstart MSPointerDown', function (e) {
        var resetItem = true,
          parents = $(e.target).parents();

        for (var i = 0; i < parents.length; i++)
          if (parents[i] == curItem[0]) resetItem = false;

        if (resetItem) curItem = false;
      });
    });
    return this;
  };

  $(document).ready(function () {});

  // Drupal behaviors go brrra

  Drupal.behaviors.Navigation = {
    attach: function () {
      $('.navigation-wrapper')
        .once('navcollapse')
        .each(function () {
          $nav = $('.navigation');

          //$nav.hide();
          $('.menu-btn').change(function () {
            $nav.slideToggle();
          });

          $nav.find('li.expanded').not('.active').find('> ul').hide();
          if ($(window).width() < 768) {
            $nav
              .find('li.expanded .dropdown-trigger')
              .on('click', function (e) {
                e.preventDefault();
                $(this).parent().next().slideToggle();
                return false;
              });
          } else {
            $('header li.expanded').doubleTapToGo();
          }
        });
    },
  };

  Drupal.behaviors.Alerts = {
    attach: function () {
      $('.alert .close').bind('click', function () {
        $(this)
          .parents('.alert')
          .animate({ opacity: 0 }, 400, function () {
            $(this).remove();
          });
      });
    },
  };

  // Drupal.behaviors.AdminMenuIcons = {
  //   attach: function () {
  //     $('.toolbar-menu-administration .toolbar-menu > li > a')
  //       .once('AdminMenuIcons')
  //       .each(function () {
  //         var data = $(this).data('drupal-link-system-path');
  //         switch (data) {
  //           case 'admin/structure/menu/manage/main':
  //             $(this).addClass('toolbar-icon-menu');
  //             break;

  //           default:
  //             break;
  //         }
  //       });
  //   },
  // };

  Drupal.behaviors.GutenbergEmbedWidth = {
    attach: function () {
      var ratioWidth = 16;
      var ratioHeight = 9;

      $('.wp-block-embed__wrapper > iframe')
        .once('GutenbergEmbedWidth')
        .each(function () {
          checkEmbedHeight($(this));
        });

      $(window)
        .once('GutenbergEmbedWidthResizeListener')
        .resize(function () {
          $('.wp-block-embed__wrapper > iframe').each(function () {
            checkEmbedHeight($(this));
          });
        });

      function checkEmbedHeight(element) {
        var width = Math.floor($(element).width());
        var currentHeight = Math.floor($(element).height());
        var expectedHeight = Math.floor((width / ratioWidth) * ratioHeight);
        if (expectedHeight !== currentHeight) {
          $(element).height(expectedHeight + 'px');
        }
      }
    },
  };

  Drupal.behaviors.CreathingGutenbergFAQ = {
    attach: function () {
      $('.wp-block-creablocks-creathing-faq')
        .once('CreathingGutenbergFAQ')
        .each(function () {
          $(this)
            .find('h4')
            .click(function () {
              $(this).parent().toggleClass('show');
            });
        });
    },
  };

  // Javascript functions go brrr
})(jQuery);
